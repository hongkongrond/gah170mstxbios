# 云轩迷你主机GA-H170MSTX-DATTO-O3 BIOS合集

#### 介绍

云轩迷你主机BIOS合集包含三部分，技嘉官方版本、云轩主机出厂版本，qlnx魔改版本。

#### 联系方式

823102060@qq.com

#### 安装教程

1.  Yunxuan和qlnx版本自带安装软件，官方版本可在BIOS中的Qflash安装。

#### 使用说明

1.  建议先查看微码再刷入。

#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
